<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function empresas(){
		$this->load->model('model_empresas');
		$data["listado"] = $this->model_empresas->listado();
		$data["informacion"] = $data["listado"];
		$this->load->view('listado',$data);
		//var_dump($data["listado"]);
	}
	public function nuevo(){
		$this->load->model('model_empresas');
		$nit = trim($_REQUEST["nit"]);
		$descripcion = trim($_REQUEST["descripcion"]);
		$direccion = trim($_REQUEST["direccion"]);
		$telefono=trim($_REQUEST["telefono"]);
		$correo=trim($_REQUEST["correo"]);
		$nombrecontacto=trim($_REQUEST["nombrecontacto"]);

		$data["listado"] = $this->model_empresas->nueva_empresa($nit,$descripcion,$direccion,$telefono,$correo,$nombrecontacto);
		header("Location: http://localhost:8888/conta/index.php/welcome/empresas");
		die();

	}
	public function borrarregistro(){
			$nit = trim($_REQUEST["nit"]);
			$this->load->model('model_empresas');
			$dato = $this->model_empresas->borrar($nit);
			var_dump($dato);
			header("Location: http://localhost:8888/conta/index.php/welcome/empresas");
			die();
	}
	public function usuedit(){
		$data["nit"]=trim($_REQUEST['nit']);
		$data["informacion"]=$data["nit"];
		$this->load->model('model_empresas');
		$this->load->view("editar_empresa",$data);

	}

	public function usueditguardar(){
		$nit=trim($_REQUEST["nit"]);
		$descripcion = trim($_REQUEST["descripcion"]);
		$direccion=trim($_REQUEST["direccion"]);
	  $telefono = trim($_REQUEST["telefono"]);
		$correo=trim($_REQUEST["correo"]);
		$nombrecontacto= trim($_REQUEST["contacto"]);
		$this->load->model('model_empresas');
		$dato = $this->model_empresas->update_emp($descripcion,$direccion,$correo,$telefono,$nombrecontacto,$nit);
		header("Location: http://localhost:8888/conta/index.php/welcome/empresas");
		die();
	}
}

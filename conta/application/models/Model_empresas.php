<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class model_empresas extends CI_Model {


    public function listado(){
        $this->load->database();

        $query = $this->db->query("select * from con_empresas");

        return $query->result();
    }

    public function nueva_empresa($nit,$descripcion,$direccion,$telefono,$correo,$nombrecontacto){
      $this->load->database();

      $query = $this->db->query("
                  insert into con_empresas (con_emp_nit,con_emp_descripcion,con_emp_direccion,con_emp_telefono,con_emp_correo,con_emp_contacto_nombre)
                     values(
                     '".$nit."',
                     '".$descripcion."',
                     '".$direccion."',
                     '".$telefono."',
                     '".$correo."',
                     '".$nombrecontacto."'
                     )");

      //return $query->result();

    }
    public function borrar($nit){
      $this->load->database();
      //$query->db->simple_query("DELETE FROM con_empresas WHERE con_emp_nit='".$nit."'");
      if (empty($nit)) {
        // code...
        $response ="no hay datos para guardar";
        return $response;
      }else{
        if ($this->db->simple_query("delete from con_empresas where con_emp_nit = '".$nit."'"))
            {
                    $response ="success";
                    return $response;

            }
            else
            {
              $response ="error";
              return $response;
            }
      }

    }
    public function update_emp($descripcion,$direccion,$correo,$telefono,$nombrecontacto,$nit){
      $this->load->database();
      if (empty($descripcion)) {
        // code...
        $response ="no hay datos para guardar";
        return $response;
      }else{
          if ($this->db->simple_query("
          UPDATE con_empresas
            SET
            con_emp_descripcion= '".$descripcion."',
            con_emp_direccion= '".$direccion."',
            con_emp_telefono= '".$telefono."',
            con_emp_correo= '".$correo."',
            con_emp_contacto_nombre= '".$nombrecontacto."'
            WHERE con_emp_nit = '".$nit."'

          "))
            {
                    $response ="success";
                    return $response;

            }
            else
            {
              $response ="error";
              return $response;
            }
      }
    }

}

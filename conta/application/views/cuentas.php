<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html>
    <head>
        <title>Proveedores</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- font de google -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style type="text/css">
          body,table,div,a,button,input
           {
              font-family: 'Open Sans', sans-serif;
              font-size: 12px;

          }

    </style>
    </head>
    <body>
        <?php  require 'menu.php'; ?>
        <br>
        <div class="row">
          <div class="col-md-4">

          </div>
          <div class="col-md-4">
            <h3 class="h3"> Listando Cuentas</h3>
          </div>
          <div class="col-md-4">

          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-1">

          </div>
          <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                  <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Codigo</th>
                          <th scope="col">Descripcion</th>
                          <th scope="col">Cuenta Padre</th>
                          <th scope="col">Acumula</th>
                          <th scope="col">Integra</tr>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                              foreach ($listado as $cuentas) {

                                ?>
                                  <tr>
                                    <td><?= $cuentas->con_cta_codigo; ?></td>
                                    <td><?= $cuentas->con_cta_descripcion; ?></td>
                                    <td><?= $cuentas->con_cta_padre; ?></td>
                                    <td><?= $cuentas->con_cta_acumula; ?></td>
                                    <td><?= $cuentas->con_cta_integra; ?></td>
                                  </tr>





                                <?php
                                // code...
                              }
                          ?>


                      </tbody>
                    </table>
                </div>
              </div>
          </div>
          <div class="col-md-1">

          </div>
        </div>
    </body>
</html>

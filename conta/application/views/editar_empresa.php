<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<html>
  <head>
    <title>
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- font de google -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style type="text/css">
          body,table,div,a,button,input
           {
              font-family: 'Open Sans', sans-serif;
              font-size: 12px;

          }
          h6 {
             font-family: 'Open Sans', sans-serif;
             font-size: 16px;

         }

    </style>
  </head>
  <body>

    <div class="row">
      <div class="col-md-12">
        <?php
            require 'menu.php';
         ?>
       </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-3">

      </div>
      <div class="col-md-6">
        <form action="usueditguardar">
        <div class="card">
          <div class="card-header">
            <h4 class="h6">Editando el Nit: <?php  echo $nit; ?></h4>
          </div>
          <div class="card-body">

                <div class="row">
                  <div class="col-md-6">
                    <label>Nit</label>
                    <input type="text" class="form-control form-control-sm" name="nit" value="<?php echo $nit; ?>" readonly>
                  </div>
                  <div class="col-md-6">
                    <label>Descripcion</label>
                    <input type="text" class="form-control form-control-sm" name="descripcion">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Direccion</label>
                    <input type="text" class="form-control form-control-sm" name="direccion" >
                  </div>
                  <div class="col-md-6">
                    <label>Telefono</label>
                    <input type="text" class="form-control form-control-sm" name="telefono">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Correo</label>
                    <input type="text" class="form-control form-control-sm" name="correo" >
                  </div>
                  <div class="col-md-6">
                    <label>Contacto</label>
                    <input type="text" class="form-control form-control-sm" name="contacto">
                  </div>
                </div>

          </div>
          <div class="card-footer">
            <input type="submit" class="btn btn-block btn-sm btn-success" value="Guardar">
          </div>
        </div>
        </form>
      </div>
      <div class="col-md-3">

      </div>
    </div>
  </body>

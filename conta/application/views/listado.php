<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<html>
  <head>
    <title>
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- font de google -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style type="text/css">
          body,table,div,a,button,input
           {
              font-family: 'Open Sans', sans-serif;
              font-size: 12px;

          }

    </style>
  </head>
  <body>

    <div class="row">
      <div class="col-md-12">
        <?php
            require 'menu.php';
         ?>

      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">

      </div>
      <div class="col-md-4">
        <h3 class="h3"> Listado de Empresas</h3>

      </div>
      <div class="col-md-4">

      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-1">

        </div>
        <div class="col-md-10">
          <div class="card">
          <div class="card-body">

            <div id="" style="overflow:scroll; height:400px;">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Nit</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Direccion</th>
                      <th scope="col">Telefono</th>
                      <th scope="col">Correo</th>
                      <th scope="col">Contacto</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($listado as $empresa) {
                        // code...
                        ?>
                        <tr>
                            <td><?= $empresa->con_emp_nit ?></td>
                            <td><?= $empresa->con_emp_descripcion ?></td>
                            <td><?= $empresa->con_emp_direccion?></td>
                            <td><?= $empresa->con_emp_telefono?></td>
                            <td><?= $empresa->con_emp_correo ?></td>
                            <td><?= $empresa->con_emp_contacto_nombre?></td>
                            <td>
                              <div class="btn-group">
                                <button type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Acciones
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="http://localhost:8888/conta/index.php/welcome/usuedit?nit=<?php echo $empresa->con_emp_nit; ?>">Editar</a>
                                  <a class="dropdown-item" href="http://localhost:8888/conta/index.php/welcome/borrarregistro?nit=<?php echo $empresa->con_emp_nit; ?>">Eliminar</a>

                                <!--  <form>
                                     <input type="button" value="Establecer" class="btn btn-link">
                                  </form>-->

                                </div>
                              </div>
                            </td>
                       </tr>

                        <?php }
                     ?>


                  </tbody>
                  </table>
              </div>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-md-4">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-outline-success btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
                Nuevo registro
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <form action="nuevo">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Ingrese datos</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                            <div class="row">
                              <div class="col-md-6">
                                  <label>Nit</label>
                                  <input type="text" class="form-control form-control-sm" name="nit">
                              </div>
                              <div class="col-md-6">
                                <label>Descripcion</label>
                                <input type="text" class="form-control form-control-sm" name="descripcion">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                  <label>Direccion</label>
                                  <input type="text" class="form-control form-control-sm" name="direccion">
                              </div>
                              <div class="col-md-6">
                                <label>Número de telefono</label>
                                <input type="text" class="form-control form-control-sm" name="telefono">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                  <label>Correo </label>
                                  <input type="text" class="form-control form-control-sm" name="correo">
                              </div>
                              <div class="col-md-6">
                                <label>Nombre del contacto</label>
                                <input type="text" class="form-control form-control-sm" name="nombrecontacto">
                              </div>
                            </div>


                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <input type="submit" class="btn btn-primary" value="Guardar">

                    </div>
                  </form>
                  </div>
                </div>
                </div>
              </div>
              <div class="col-md-4">

              </div>
              <div class="col-md-4">
                <form>

                </form>
              </div>
            </div>
          </div>
        </div>

        </div>
        <div class="col-md-1">

        </div>
    </div>
  </body>

</html>
